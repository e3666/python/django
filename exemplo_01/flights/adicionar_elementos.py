from flights.models import Flight, Airport

a = Airport(code='AAA', city='cidade_a')
a.save()
b = Airport(code='BBB', city='cidade_b')
b.save()

f1 = Flight(origin=a, destination=b, duration=45)
f1.save()
f2 = Flight(origin=b, destination=a, duration=45)
f2.save()

