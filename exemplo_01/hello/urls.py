from django.urls import path
from hello import views

urlpatterns = [
    path("", views.index, name="index"),
    path("lucas", views.lucas, name="lucas"),
    path("<str:name>", views.greet, name="greet"),
    path("templ/<str:name>", views.greet_with_template, name="greet_2"),
]
